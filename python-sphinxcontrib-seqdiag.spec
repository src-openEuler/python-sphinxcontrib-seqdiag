%global _empty_manifest_terminate_build 0
Name:           python-sphinxcontrib-seqdiag
Version:        3.0.0
Release:        1
Summary:        Sphinx "seqdiag" extension
License:        BSD
URL:            https://github.com/blockdiag/sphinxcontrib-seqdiag
Source0:        https://files.pythonhosted.org/packages/9d/e6/5430aa6d8750337e14555d94bf9fac5ae7e1f0ac9198cd0c2c59a2e226b7/sphinxcontrib-seqdiag-3.0.0.tar.gz
BuildArch:      noarch
%description
A sphinx extension for embedding sequence diagram using seqdiag.

%package -n python3-sphinxcontrib-seqdiag
Summary:        Sphinx "seqdiag" extension
Provides:       python-sphinxcontrib-seqdiag
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-blockdiag
BuildRequires:  python3-seqdiag
BuildRequires:  python3-sphinx
# General requires
Requires:       python3-blockdiag
Requires:       python3-seqdiag
Requires:       python3-sphinx
%description -n python3-sphinxcontrib-seqdiag
A sphinx extension for embedding sequence diagram using seqdiag.

%package help
Summary:        Sphinx "seqdiag" extension
Provides:       python3-sphinxcontrib-seqdiag-doc
%description help
A sphinx extension for embedding sequence diagram using seqdiag.

%prep
%autosetup -n sphinxcontrib-seqdiag-%{version}

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .


%files -n python3-sphinxcontrib-seqdiag -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Jul 04 2022 OpenStack_SIG <openstack@openeuler.org> - 3.0.0-1
- Upgrade package python3-sphinxcontrib-seqdiag to version 3.0.0

* Wed May 11 2022 yangping <yangping69@h-partners> - 2.0.0-2
- License compliance rectification

* Tue Jan 05 2021 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
